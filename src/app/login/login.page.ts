import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public signInForm: FormGroup;
  public isSubmitted = false;

  constructor(
    public formBuilder: FormBuilder,
    private loader: LoadingController,
    public router: Router,
  ) { }

  ngOnInit() {
    console.log('ngOnInit LoginPage');
    this.initSignInForm();
  }

  initSignInForm(){
    this.signInForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  get errorControl() {
    return this.signInForm.controls;
  }

  async signIn(){
    this.isSubmitted = true;
    if (!this.signInForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      console.log(this.signInForm.value);
      let loading  = await this.loader.create({
        message: "Please wait...",
        duration: 2000
      });
      await loading.present();
      const { username, password } = this.signInForm.value;
      let params = {
        username: username,
        password: password
      }
      this.router.navigate(['tabs'], { state : params});
      await loading.dismiss();
    }
  }

}
