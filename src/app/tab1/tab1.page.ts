import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild("slides") slides: IonSlides;
  username: string;
  password: string;
  mockData = [1,2,3,4,5,6,7,8,9,10];
  onIndexCard = 0;
  slidesOptions = {
    initialSlide: 0,
    slidesPerView: 1.2,
    spaceBetween: 2.5,
  };

  constructor(
    private router: Router,
    ) {
      let extras = this.router.getCurrentNavigation().extras.state;
      console.log("get extras:", extras );
      this.username = extras.username;
    }


  async slideChanged(event ? : any) {
    console.log("---> slideChanged", event);
      let currentIndex = await this.slides.getActiveIndex()
      console.log('Changed slide index: '+ currentIndex ,'begin',await this.slides.isBeginning(),'end',await this.slides.isEnd(),'of',await this.slides.length());
      if(await this.slides.isEnd() && !await this.slides.isBeginning()){
        currentIndex = await this.slides.length() - 1
      }
      if(currentIndex != this.onIndexCard){
        this.onIndexCard = currentIndex
      }
  }
}
